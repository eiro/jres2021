---
vim: et ts=4 sts=4 sw=4
title: Article JRES 2021 (nom à trouver)
keywords: [convivialité, green it, ]
author: [mch, ecoinfo]
---

# rapide rappel

* les pollutions et leurs conséquences
* limits: la rarefication

# parcours perso (si redaction pas collectiv )

    assos -> JRES -> {Renater Ecoinfo}

    => parler à tout le monde
    => respecter les 50 nuances de green ?

# problème

proposer des solutions techniques => impasse

invoquer
* les habitudes et le faible niveau technique des usagers
* le manque de moyens/compétences sans prendre de dispositions
* satisfaction par des économies marginales effectuées sur des SI existants
  qui sont une démonstration du problème

les non-dits:
* la destruction de modèles économiques et de cultures d'entreprise
  => résistance consciente ou non.

# plaidoyer pour le vert foncé

## convivialité numérique

=> suckless, illich ...

=> exemple (stack a produire)

# des pistes pour le SI

* reproduire de la formation, de l'accompagnement et du support
      => exemple négatif du webmail: liberté+choix
      => exemple positif: COP étudiante
* apprendre les *vraies bases aux élèves*

## travaillons ensemble (chacun son rythme/public)

=> introduction d'alternatives vertes au contenu ?

# prise de conscience ?

(reparler du dernier café)





